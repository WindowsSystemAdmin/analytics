{% docs pump_marketing_contact %}

a copy of mart_marketing_contact for sending to Marketo for use in email campaigns

{% enddocs %}

{% docs pump_subscription_product_usage %}

A copy of `subscription_product_usage_data` model for sending to Salesforce

{% enddocs %}